class node{
    constructor(data){
        this.data = data
        this.next = null
    }
}

const a = new node(1)
const b = new node(2)
const c = new node(3)
const d = new node(4)

a.next = b
b.next = c
c.next = d


const printLinkedList = (head) => {
    let current = head;

    while(current != null){
        console.log(current.data)
        current = current.next
    }
}

printLinkedList(a)