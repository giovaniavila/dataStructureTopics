# Listas encadeadas

As listas encadeadas são formas de trabalharmos com vetores de forma com que facilite a 
manipulaçâo dos elementos, seja em um método de inserção ou remoção.

## linked list vs array
as principais vantagens sobre uma array comum é o seu <strong>tamanho dinâmico </strong>, que permite com que a lista aumenta conforme necessário. Além de que também estão dispersos na memória.

#### desvantagens
- acesso sequencial
- uso de mais memória
